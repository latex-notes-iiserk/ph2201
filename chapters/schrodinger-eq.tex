\chapter{Schr\"odinger Equation}

\noindent Recall \cref{eq:operator-SE}, if we expand this operator equation we get ``\vocab{Schr\"odinger Equation}'' (SE).
\begin{equation}
    \qty(-\frac{\hbar^2}{2 m} \laplacian + V) \Psi = i \hbar \pdv{t} \Psi \label{eq:schrodinger-eq}
\end{equation}
A partial differential equation (PDE) is generally not easy to solve.

\paragraph{Method of Separation of variables:} Consider the case of \(1+1\)D. Let \(\Psi(x, t)\) be a solution of the Schr\"odinger equation. Assume \(\Psi\) is separable,
\begin{gather}
    \Psi(x, t) = \psi(x) T(t) \label{eq:separable-sol}
    \intertext{using this form in \cref{eq:schrodinger-eq},}
    \frac{1}{T(t)} i \hbar \dv{T}{t} = \frac{1}{\psi(x)} \qty[-\frac{\hbar^2}{2 m} \dv[2]{\psi}{x} + V(x) \psi(x)] = E
    \intertext{for some \(E \in \C\)}
    \underbrace{\boxed{\frac{1}{T} ~ i \hbar \dv{T}{t} = E}}_{\color{purple} \text{Time-dependent part}}, \qquad \qquad \qquad \underbrace{\boxed{-\frac{\hbar^2}{2 m} \dv[2]{\psi}{x} + V(x) \psi(x) = E \psi(x)}}_{\color{purple} \text{Time-independent part}}
\end{gather}

\section{Properties of wave-function}\label{sec:prop-wave}

This section will list all the basic properties of a valid wave function.

\begin{enumerate}
    \item Wave functions are complex single-valued functions.

          Since the SE doesn't impose any condition on the co-domain of the function, it is good to have a complex function. However, we require the wave function to be a mathematical function, as we will see that the square of the norm of the wave function represents the probability density function.

    \item Wave function is square integrable.

    \item Wave functions are continuous (and space derivatives are continuous for finite potentials,  not in general)
\end{enumerate}

\section{Born Interpretation}
We know that Maxwell's Equation of Electromagnetism concludes an equation which implies that \textbf{charge is a conserved quantity} known as ``\vocab{Continuity Equation},'' \ie
\begin{equation}
    \pdv{\rho}{t} + \div{\vb{J}} = 0
\end{equation}
Consider a volume $V$ with a surface denoted by $S$, we can write the rate of change of charge $Q$ in the volume as,
\begin{align*}
    \dv{t} \int\limits_{V} \dd[3]{\vb{r}} ~ \rho(\vb{r}, t) & = \int\limits_{V} \pdv{\rho}{t} \dd[3]{\vb{r}}
    \shortintertext{Using continuity equation, we have}
    \dv{t} \int\limits_{V} \dd[3]{\vb{r}} ~ \rho(\vb{r}, t) & = - \int\limits_{V} \dd[3]{\vb{r}} \qty(\div{\vb{J}})
    \shortintertext{From Divergence Theorem,}
                                                            & = - \int\limits_{S} \vb{J} \vdot \dd{\vb{S}}
    \shortintertext{So if the total current density flux through the surface is zero \ie}
    \int\limits_{S} \vb{J} \vdot \dd{\vb{S}}                & = 0
    \shortintertext{then, the total charge $Q$ enclosed in the volume is conserved \ie}
    \dv{Q}{t}                                               & = 0
\end{align*}
Now consider full Shr\"odinger equation and the complex conjugate of the same (assuming that potential is real-valued),
\begin{equation*}
    i \hbar \pdv{\Psi}{t} = - \frac{\hbar^2}{2 m} \laplacian{\Psi} + V \Psi \qquad ; \qquad i \hbar \pdv{\Psi^*}{t} = - \frac{\hbar^2}{2 m} \laplacian{\Psi^*} + V \Psi^*
\end{equation*}
Multiply these equations by $\Psi$ and $\Psi^*$ respectively and then subtract these equations, we have
\begin{gather}
    i \hbar \qty(\Psi \pdv{\Psi^*}{t} - \Psi^* \pdv{\Psi}{t}) = -\frac{\hbar^2}{2 m} \qty(\Psi^* \laplacian{\Psi} - \Psi \laplacian{\Psi^*}) \nonumber \\
    \implies \qquad \pdv{t} (\Psi^* \Psi) = - \div[\frac{\hbar}{2 m i} \qty(\Psi^* \grad{\Psi} - \Psi \grad{\Psi^*})] \label{eq:continuity-prob}
\end{gather}
Define,
\begin{equation*}
    \rho := \Psi^* \Psi = \abs{\Psi}^2 \qquad ; \qquad \vb{J} := \frac{\hbar}{2 m i}\qty(\Psi^* \grad{\Psi} - \Psi \grad{\Psi^*})
\end{equation*}
The \cref{eq:continuity-prob} with these new definitions, became a continuity equation (for wave function),
\begin{equation*}
    \pdv{t}\rho + \div{\vb{J}} = 0
\end{equation*}
If the flux of this new current through a surface $S$ which encloses a closed volume $V$ is zero then
\begin{equation*}
    \int\limits_{V} \dd[3]{\vb{r}} \Psi^* \Psi ~\text{is conserved!}
\end{equation*}
\textbf{Max Born (1926):} gave a statistical interpretation, $\rho = \Psi^*(\vb{r}, t) \Psi(\vb{r}, t)$ is the \textbf{probability density} of finding the particle at point $(\vb{r}, t)$.

Since in statistics, it is a convention to have a total probability of $1$, extending this convention in QM, we demand that the wave function should be normalizable (so we can have),
\begin{equation*}
    \int \dd[3]{\vb{r}} \Psi^* \Psi = 1.
\end{equation*}

\section{Hilbert Space}

Consider there are two solutions $\Psi_1$ and $\Psi_2$ of Schro\"odinger equation \ie
\begin{equation*}
    i \hbar \pdv{\Psi_1}{t} = \widehat{H}\Psi_1 \qquad ; \qquad i \hbar \pdv{\Psi_2}{t} = \widehat{H}\Psi_2
\end{equation*}
Now we discuss some of the algebraic structure that comes naturally from these solutions. In the end, we try to argue that this set of solutions forms an inner product space namely ``\vocab{Hilbert Space}.''

\subsection{Superposition}
\begin{claim}
    Any arbitrary linear combination of $\Psi_1$ and $\Psi_2$ is also a solution of the SE.
\end{claim}
\begin{proof}
    Consider a linear combination as $\Psi = c_1 \Psi_1 + c_2 \Psi_2$ for some $c_1, c_2 \in \C$.
    \begin{align*}
        i \hbar \pdv{\Psi}{t} & = c_1 i \hbar \pdv{\Psi_1}{t} + c_2 i \hbar \pdv{\Psi_2}{t} \\
                              & = c_1 \widehat{H}\Psi_1 + c_2 \widehat{H}\Psi_2             \\
                              & = \widehat{H}\qty(c_1 \Psi_1 + c_2 \Psi_2)                  \\
                              & = \widehat{H}\Psi
    \end{align*}
    This result is known as the principle of linear superposition of the wave function.
\end{proof}
This proves that the set of solutions forms a linear vector space \ie it satisfies all the axioms of a vector space that it is an Abelian Group with operation $+$ and a well-defined scalar multiplication.

\subsection{Inner Product}
We can define a sesquilinear form on this linear vector space as
\begin{equation*}
    \bilinear{\Psi | \Phi} = \int \dd{\tau} \Psi^* \Phi
\end{equation*}
Due to our statistical interpretation of wave function, we have to have
\begin{equation*}
    \bilinear{\Psi | \Psi} = \int \dd{\tau} \Psi^* \Psi < \infty
\end{equation*}
Since the integrand is positive, the sesquilinear form is positive-definite by definition. So we can define the norm for this vector space \ie
\begin{equation*}
    \norm{\Psi} = \sqrt{\bilinear{\Psi | \Psi}}
\end{equation*}
This implies that all the wave functions are square-integrable. This set of solutions with a defined inner product is known as ``Hilbert Space.''

\section{Infinite Potential Well}

It is a toy model for something. Consider a particle trapped inside a box confined by potentials (as walls). The simplest possible case is the following potential:
\begin{equation*}
    V(x) := \begin{cases}
        0      & 0 \le x \le a    \\
        \infty & \text{otherwise}
    \end{cases}
\end{equation*}
With this potential, we can have a classical analysis (force analysis) regarding the motion of the particle.
\begin{enumerate}
    \item The particle is free for all $x \in [0, a]$.
          \begin{equation*}
              \vb{F} = - \grad{V} = \vb{0} \qquad 0 \le x \le a
          \end{equation*}

    \item The particle experiences an infinite force if it tries to move to the right at $x = a$ or to the left at $x = 0$.
          \begin{gather*}
              \vb{F}(a^+) = - \qty[\lim_{h \to 0} \frac{V(a + h) - V(a)}{h}] = \infty \qquad \vb{F}(a^-) = - \qty[\lim_{h \to 0} \frac{V(a) - V(a - h)}{h}] = 0 \\
              \vb{F}(0^+) = - \qty[\lim_{h \to 0} \frac{V(0 + h) - V(0)}{h}] = 0 \qquad \vb{F}(0^-) = - \qty[\lim_{h \to 0} \frac{V(0) - V(0 - h)}{h}] = \infty
          \end{gather*}
\end{enumerate}
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.425\textwidth]{ipw.png}
    \caption{Infinite Potential Well}
    \label{fig:ipw}
\end{figure} \noindent

\subsection{Time Independent Schr\"odinger Equation}

From our classical notion, we can deduce that the wave function should be zero where the potential is infinite \ie
\begin{equation*}
    \psi(x) =
    \begin{cases}
        0 & x \in (-\infty, 0) \\
        0 & x \in (0, \infty)
    \end{cases}
\end{equation*}
Now let's focus on the non-trivial solution (in an interesting region),
\begin{equation*}
    \widehat{H} \psi(x) = E \psi(x) \qquad \implies \qquad -\frac{\hbar^2}{2 m} \dv[2]{x} \psi = E \psi
\end{equation*}
Since we are dealing with a single dimension, the PDE is reduced to second-order ODE. For this specific ODE, we know the general solution:
\begin{equation*}
    \psi(x) = A e^{i \kappa x} + B e^{-i \kappa x}, \qquad \kappa^2 = \frac{2 m E}{\hbar^2}
\end{equation*}
We have two unknown constants $A$ and $B$ that need to be determined. Recalling some properties of the wave-function from \cref{sec:prop-wave}, we at least require continuity of wave function at boundaries.
\begin{align*}
     & \lim_{x \to 0^+} \psi(x) = \lim_{x \to 0^-} \psi(x) \quad \implies \quad A + B = 0                                \\
     & \lim_{x \to a^+} \psi(x) = \lim_{x \to a^-} \psi(x) \quad \implies \quad A e^{i \kappa a} + B e^{-i \kappa a} = 0
\end{align*}
Using the first equation, we have
\begin{align*}
     & \qquad A\qty(e^{i \kappa a} - e^{-i \kappa a}) = 0                      \\
     & \implies \quad e^{2 i \kappa a} - 1 = 0                                 \\
     & \implies \quad e^{2 i \kappa a} = 1 = e^{i (2 \pi n)}                   \\
     & \implies \quad \kappa a = \pi n, \quad n \in \Z_{> 0}                   \\
     & \implies \quad \kappa^2 = \frac{n^2 \pi^2}{a^2} = \frac{2 m E}{\hbar^2}
\end{align*}
This last equation proves that energy is quantized in this system \ie only a certain value of energy is allowed.
\begin{equation*}
    E_n = \frac{\hbar^2 \pi^2}{2 m a^2} n^2, \qquad n \in \Z_{> 0}
\end{equation*}
These energy levels are known as ``Eigen State Energy.'' Using this result we can write the wave function corresponding to the $n^{\text{th}}$ eigenstate energy.
\begin{equation*}
    \psi_n(x) = A \qty(e^{i (n \pi/a) x} - e^{-i (n \pi/a) x}) = C \sin(\frac{n \pi}{a} x)
\end{equation*}
Observe that the specified condition is also satisfied when $n = 0$, but in that case, our wave function becomes trivially zero (in full range). So the minimum energy for this specified particles is when $n = 1$, known as ``zero-point energy.''

\subsection{Time Dependent Schr\"odinger Equation}
For a given choice of $E$, we have a specific solution for the time-dependent solution,
\begin{equation*}
    i \hbar \dv{T}{t} = T E_n \quad \implies \quad T_n(t) = T_\circ e^{-i (E_n/\hbar) t}
\end{equation*}
With this solution, we can construct a full solution to the full Schr\"odinger equation by
\begin{equation*}
    i \hbar \pdv{\Psi}{t} = \widehat{H}\Psi \qquad \implies \qquad \Psi_n(x, t) = \psi_n(x) T_n(t)
\end{equation*}
Putting all this together, we can write the general solution as,
\begin{equation*}
    \Psi_n(x, t) = \Psi_\circ e^{-i (E_n/\hbar) t} \sin(\frac{n \pi}{a} x), \qquad E_n = \frac{\hbar^2 \pi^2 n^2}{2 m a^2}
\end{equation*}
Define the wave number $(k_n)$ and angular frequency $(\omega_n)$ as
\begin{equation*}
    k_n := \frac{n \pi}{a} \qquad ; \qquad \omega_n := \frac{E_n}{\hbar} = \frac{h \pi^2 n^2}{2 m a^2}
\end{equation*}
With this, we can rewrite the solution as,
\begin{align*}
    \Psi_n & = \Psi_\circ e^{-i (E_n/\hbar) t} \qty(\frac{e^{i (n \pi/a) x} - e^{-i (n \pi/a) x}}{2 i})                                                                    \\
           & = \underbrace{c_1 e^{i(k_n x - \omega_n t)}}_{\text{wave moving to the right}} + \underbrace{c_2 e^{-i(k_n x + \omega_n t)}}_{\text{wave moving to the left}}
\end{align*}

\begin{exercisebox}
    \begin{exercise}[Assignment-04/Ex-01]
        Show that for the infinite square well potential, the normalized energy eigenstate (time-independent) can be expressed as
        \begin{equation*}
            \psi_n = \sqrt{\frac{2}{a}} \sin(\frac{n \pi}{a} x)
        \end{equation*}
    \end{exercise}
\end{exercisebox}

\begin{exercisebox}
    \begin{exercise}[Assignment-04/Ex-02]
        Show that all energy eigenstates form an orthonormal set of vectors \ie
        \begin{equation*}
            \bilinear{\psi_m, \psi_n} = \delta_{m n}
        \end{equation*}
        for $\psi_n = \sqrt{2/a} \sin(n \pi x/a)$
    \end{exercise}
\end{exercisebox}

\section{Finite Depth Potential Well}

\section{Quantum Harmonic Oscillator}
