\chapter{Mathematical Prerequisites}

\section{Vector Space}

Consider the linear vector space \(\R^2\). An element of this set is called a ``vector'' \(\vb{v} \in V\). We can decompose the vector in corresponding coordinate vectors as:
\begin{equation*}
    \vb{v} = v_x \i + v_y \j
\end{equation*}

\paragraph{Rotation by angle \(\theta\) about \(z\)-axis:}
Consider that we rotate this given vector by an angle \(\theta\).
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.3\textwidth]{rotation.png}
    \caption[short]{Rotating a vector \(\vb{v}\) by an angle of \(\theta\) about \(z\)-axis.}
    \label{fig:rotation}
\end{figure}
Coordinate of the new rotated vector is given by
\begin{gather*}
    v_x' = v_x \cos\theta + v_y \sin\theta \qquad \qquad \qquad \qquad v_y' = -v_x \sin\theta + v_y \cos\theta \\
    \mqty(v_x' \\ v_y') = \mqty(\cos\theta & \sin\theta \\ -\sin\theta & \cos\theta) \mqty(v_x \\ v_y) \qquad \implies \qquad \vb{v}' = R(\theta) \vb{v}
\end{gather*}

\subsection{Operators}
\begin{definition}[Operator]
    \(\hat{O}\) is a linear map from a linear vector space to itself \ie \(\vb{v}' = \hat{O} \vb{v}\) such that \(\vb{v}, \vb{v}' \in V\).
\end{definition}
We can see that \(R(\theta)\) is an example of an operator.
\begin{example}[Introduction to eigenvalue equation]
    Consider the operation of rotation by an angle of \(\pi\), and the corresponding operator will be given by \(R(\theta = \pi)\).
    \begin{gather*}
        \vb{v}' = R(\pi) \vb{v} = -\vb{v} \\
        R(\pi) \vb{v} = \lambda \vb{v}, \qquad \qquad \text{for \(\lambda = -1\)}
    \end{gather*}
\end{example}
An operator equation of the following form is called an \vocab{eigenvalue equation}.
\begin{equation}
    \hat{O} \psi = \lambda \psi
\end{equation}
\begin{itemize}[$\cdot$, noitemsep]
    \item \(\hat{O}\) is an operator on vector space \(V\).
    \item \(\psi\) is a vector from a given vector space \(V\), also known as eigenvector.
    \item \(\lambda \in \C\) is called eigenvalue.
\end{itemize}

\section{Maxwell's Wave Equation}

Let \(\Psi \in \qty{\vb{E}, \vb{B}}\), and in general \(\Psi = \Psi(\vb{r}, t)\). We know Maxwell's wave equation as
\begin{equation}
    \laplacian{\Psi} = \frac{1}{c^2} \pdv[2]{\Psi}{t} \label{eq:maxwell-wave-eq}
\end{equation}
The general solution of \cref{eq:maxwell-wave-eq} is given by
\begin{equation}
    \Psi = \Psi(\vb{r}, t) = \Psi_\circ e^{i(\vb{k} \vdot \vb{r} - \omega t)} \label{eq:plane-wave-sol}
\end{equation}
This oscillating solution (a complex-valued function \(\Psi(\vb{r}, t)\)) is called an ``Electromagnetic wave'' with angular frequency \(\omega\).

\subsection{Energy Operator}

The wave solution in \cref{eq:plane-wave-sol} represents a light quanta of energy
\begin{equation}
    E = h \nu = \hbar \omega
\end{equation}
Consider a differential operator \(i \hbar \pdv{t}\)
\begin{gather}
    i \hbar \pdv{\Psi}{t} = (i \hbar) (-i \omega) \Psi = (\hbar \omega) \Psi \nonumber \\
    \boxed{i \hbar \pdv{\Psi}{t} = (\hbar \omega) \Psi = E \Psi}
\end{gather}
Define an operator (energy operator) by
\begin{equation}
    \widehat{E} = i \hbar \pdv{t} \qquad \implies \qquad \widehat{E} \Psi = E \Psi
\end{equation}
This is an eigenvalue equation where the eigenvalue is the energy of the corresponding light quanta. Therefore, the operator \(\widehat{E}\) represents the energy operator.

\subsection{Momentum Operator}

From Planck's hypothesis, the momentum of a light quanta is given by
\begin{gather}
    p = \frac{h}{\lambda} = \frac{h}{2 \pi} \cdot \frac{2 \pi}{\lambda} = \hbar k
    \intertext{In case of 3D plane wave,}
    \vb{p} = \hbar \vb{k}
\end{gather}
To device a momentum operator for wave solution, we must get momentum as an eigenvalue for some operator. Let us define momentum operator for \(x\)-direction by \(\hat{p}_x = -i \hbar \pdv{x}\).
\begin{gather}
    -i \hbar \pdv{\Psi}{x} = (-i \hbar) (i k_x) \Psi = (\hbar k_x) \Psi \nonumber \\
    \boxed{i \hbar \pdv{\Psi}{x} = (\hbar k_x) \Psi = p_x \Psi}
\end{gather}
Extending this definition, in the case of 3D wave
\begin{gather}
    \vu{p} = -i \hbar \grad \\
    \vu{p} = \hbar \qty(k_x \i + k_y \j + k_z \k) \Psi
\end{gather}

\section{Building Schr\"odinger Equation}

The energy of a particle, in general, is given by
\begin{equation}
    E = \frac{\abs{\vb{p}}^2}{2 m} + V \label{eq:total-energy}
\end{equation}
Let's define another operator (namely Hamiltonian operator \(\widehat{H}\)) for total energy (motivated by classical expression of energy \cref{eq:total-energy}). Hamiltonian operator corresponding to the particle can expressed as
\begin{equation}
    \widehat{H} = \frac{\vu{p}^2}{2 m} + \widehat{V} = -\frac{\hbar^2}{2 m} \laplacian + V(\vb{r}, t)
\end{equation}
As \textbf{de Broglie}'s hypothesis (\textbf{1942}) mentions, all particles must also have wave-like properties since light has both wave-like and particle-like properties. Under this hypothesis, consider the particle is described by some \textcolor{purple}{wave function} \(\Psi\), so the following energy relation must hold.
\begin{equation}
    \widehat{E} \Psi = \widehat{H} \Psi \label{eq:operator-SE}
\end{equation}