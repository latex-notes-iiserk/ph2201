\chapter{Origins of Quantum Mechanics}

In this section, we will go through the historical development of Quantum Mechanics as a field of physics.

\section{Classical Physics}

We refer to the part of physics governed by Newton's Laws of Motion (referred to as Classical Mechanics sometimes) or Classical Electrodynamics as ``\vocab{Classical Physics}.''

\subsection{Classical Mechanics}

Classical mechanics comprises many theories from many physicists, but we can condense them into two theories proposed by \textbf{Issac Newton} in 1687.

\begin{list}{}{}
    \item[\bfseries Laws of Motion:] They are the collection of three laws that explain the dynamics of almost all macroscopic objects.
        \begin{equation}
            \vb{F}_{\text{ext}} = \dv{\vb{p}}{t}
        \end{equation}

    \item[\bfseries Law of Gravitation:] It was the mathematical equation that calculates the gravitational force between two objects of mass \(m_1, ~m_2\) respectively separated by a distance of \(r\).
        \begin{figure}[H]
            \centering
            % \includesvg[width=0.5\textwidth]{diagram.svg}
            \includegraphics[width=0.25\textwidth]{gravity.png}
            \begin{equation}
                \vb{F} = - \frac{G m_1 m_2}{r^2} ~\vu{r}
            \end{equation}
            \caption{Two objects of mass \(m_1\) and \(m_2\) under the influence of their mutual gravity when separated by a distance \(r\).}
            \label{fig:hello}
        \end{figure}
\end{list}

\subsection{Classical Electrodynamics}

Multiple physicists have explored the field of electromagnetism across centuries, and we will go through all the laws in the form of a timeline.

\begin{list}{}{}
    \item[\bfseries Charles Coulomb (1785):] formulated ``the Law of Electrostatic force,'' which calculates the force experienced by a charge in the vicinity of another charge. Consider two charges \(q_1\) and \(q_2\) which are separated by a distance of \(r\).
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.25\textwidth]{coulomb.png}
            \begin{equation}
                \vb{F} = \frac{1}{4 \pi \eps_\circ} \frac{q_1 q_2}{r^2} ~\vu{r}
            \end{equation}
            \caption{Two point charges \(q_1\) and \(q_2\) separated by a distance of \(r\), which are experiencing electrostatic force due to their fields.}
            \label{fig:coulomb}
        \end{figure}

    \item[\bfseries Amp\`ere (1823):]
        \begin{equation}
            \oint_{C} \vb{B} \vdot \dd{\vb{r}} = \mu_\circ I_{\text{enc}}
        \end{equation}
    \item[\bfseries Faraday (1831):]
        \begin{equation}
            \mathcal{E} = - \dv{\Phi_B}{t}
        \end{equation}
    \item[\bfseries Gauss (1835):]
        \begin{equation}
            \div{\vb{E}} = \frac{\rho}{\eps_\circ}
        \end{equation}
\end{list}
Later, in \textbf{1862}, \textbf{James Clerk Maxwell} unified the theory of electrostatics and magnetism and formalised ``\textbf{Law of Electrodynamics}.''
\begin{gather}
    \div{\vb{E}} = \frac{\rho}{\eps_\circ} \\
    \div{\vb{B}} = 0 \\
    \curl{\vb{E}} = - \pdv{\vb{B}}{t} \\
    \curl{\vb{B}} = \mu_\circ \qty[\vb{J} + \eps_\circ \pdv{\vb{E}}{t}]
\end{gather}
% \setlist{nolistsep}
\begin{multicols}{2}
    \begin{itemize}[$\cdot$, noitemsep]
        \item \(\vb{E} =\) Electric field
        \item \(\vb{B} =\) Magnetic field
        \item \(\vb{J} =\) current density
        \item \(\rho =\) charge density
        \item \(\eps_\circ =\) permittivity of free space
        \item \(\mu_\circ =\) permeability of free space
    \end{itemize}
\end{multicols}

\noindent{\bfseries Lorentz (1895):} formulated the ``law of electromagnetic force''
\begin{equation}
    \vb{F} = q \qty[\vb{E} + \vb{v} \cp \vb{B}]
\end{equation}

\subsection{Theory of Relativity}

By the end of the 19\textsuperscript{th} century, much experimental evidence pointed out a big hole in the theory of relativity (first formalized by \textbf{Galileo Galilei}), upon which Newton's Laws of motion were built.

\noindent In \textbf{1905}, \textbf{Albert Einstein} recified all the loopholes with the ``Theory of Special Relativity''
\begin{equation}
    \mqty(ct' \\ x' \\ y' \\ z') = \mqty(
    \gamma & - \gamma \beta & 0 & 0 \\
    - \gamma \beta & \gamma & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1 \\
    ) \mqty(ct \\ x \\ y \\ z)
\end{equation}
However, this theory was incomplete as it only answers the relativity issue if the observer is an inertial observer.

\noindent In \textbf{1915}, Einstein addressed this problem with a new formulation known as the ``Theory of General Relativity.''

\section{Three Unanswered Problems}

This section will discuss three critical failures of classical physics that led to the birth of Quantum Mechanics. Following are the phenomena that were not explained by classical physics:
\begin{enumerate}
    \item \uline{Black Body Radiation:} Max Planck (1900)
    \item \uline{Photoelectric Effect:} Albert Einstein (1905)
    \item \uline{Spectral Lines in photo-emission:} Niels Bohr (1913)
\end{enumerate}
The first thing we can observe in all these phenomena is that they are concerned with the properties of light!!

\subsection*{What is Light?}
\noindent Before addressing these phenomena, we should answer, ``\textbf{What is light?}''\\[1.2ex]
Consider a volume \(\Gamma \subseteq \R^3\) enclosed by a surface \(\Sigma\). Let \(\Gamma\) is a subset of free space (\ie \(\vb{J} = 0\) and \(\rho = 0\)). Using these properties, the corresponding Maxwell's equation of electrodynamics will be as follows:
\begin{gather}
    \div{\vb{E}} = 0 \\
    \div{\vb{B}} = 0 \\
    \curl{\vb{E}} = - \pdv{\vb{B}}{t} \label{eq:faraday-light} \\
    \curl{\vb{B}} = \meo \pdv{\vb{E}}{t} \label{eq:ampere-light}
    \intertext{If we take the curl of \cref{eq:faraday-light}, we get}
    \curl(\curl{\vb{E}}) = - \curl(\pdv{\vb{B}}{t}) \\
    \grad(\cancelto{0}{\div{\vb{E}}}) - \laplacian{\vb{E}} = - \pdv{t}(\curl{\vb{B}})
    \intertext{substituting from \cref{eq:ampere-light}, we get}
    \boxed{\laplacian{\vb{E}} = \meo \pdv[2]{\vb{E}}{t}}
\end{gather}

\begin{exercisebox}
    \begin{exercise}[Assignment-01/Ex-01] \label{ex:magnetic-wave}
        Show that magnetic field ($\vb{B}$) also has the same form.
        \begin{equation}
            \laplacian{\vb{B}} = \meo \pdv[2]{\vb{B}}{t}
        \end{equation}
    \end{exercise}
\end{exercisebox}
We see both \(\vb{E}\) and \(\vb{B}\) satisfy a wave-equation of the form
\begin{equation*}
    \boxed{\laplacian{f} = \frac{1}{v^2} \pdv[2]{f}{t}}, \qquad \qquad \qq{\(v\) is speed of propagation of the wave}
\end{equation*}

\paragraph{Speed of Electromagnetic wave:}
\begin{equation}
    v = \frac{1}{\sqrt{\meo}} = 3 \times 10^8 ~\mathrm{m/sec}
\end{equation}
In \textbf{1676}, \textbf{Ole Roemer} measured the speed of light (say \(c\)) through astronomical observation. It turned out that
\begin{equation*}
    v \approx c
\end{equation*}
Maxwell, in 1864, conjectured that light is also an electromagnetic wave.

\subsection{Black Body Radiation}

Every physical body spontaneously emits electromagnetic radiation. This radiation depends on the body's temperature \(T\).

\begin{definition}[Spectral Energy Density]
    Energy density of radiation having frequency \(\nu\) to \(\nu + \dd{\nu}\).
\end{definition}

\begin{list}{}{}
    \item[\bfseries Wilhem Wien (1896):]
        \begin{equation}
            \U_\nu = \frac{8 \pi h}{c^3} \nu^3 \exp(-\frac{h \nu}{k_B T})
        \end{equation}
        \begin{itemize}[$\cdot$, noitemsep]
            \item \(k_B =\) Boltzmann constant.
            \item \(h =\) a constant needed to make \((h \nu / k_B T)\) dimensionless!
        \end{itemize}
        This formula applies to high-frequency (short wavelength) \ie \(h \nu \gg k_B T\). It doesn't work for low frequency \ie \(h \nu \ll k_B T\).

    \item[\bfseries Rayleigh-Jeans (1900):]
        \begin{equation}
            \U_\nu = \frac{8 \pi (k_B T)}{c^3} \nu^2
        \end{equation}
        This formula applies to low-frequency (long wavelength) \ie \(h \nu \ll k_B T\). It doesn't work for high frequency \ie \(h \nu \gg k_B T\).

    \item[\bfseries Max Planck (1900):] Wrote down an empirical formula that matches Wien's formula but also works for low frequency.
        \begin{equation}
            \U_\nu = \frac{8 \pi h}{c^3} \frac{\nu^3}{\exp(\frac{h \nu}{k_B T}) - 1}
        \end{equation}
        We can retrieve both formulas in the appropriate range.
\end{list}
In 1901, Planck proposed the notion of a ``hypothetical oscillator'' to describe the black body radiation of frequency \(\nu\), having energy as integer Multiple of \(h \nu\).\\
The idea of `quanta' is a mathematical device that leads to a single formula and `need not exist somewhere in nature.'

Planck proposed that the energy of a monochromatic beam of radiation with frequency \(\nu\) should be of the form.
\begin{equation}
    E = N h \nu = N \qty(\frac{h}{2 \pi}) \qty(2 \pi \nu) = N \hbar \omega
\end{equation}
Using this notion of energy in radiation, we can redefine \uline{energy flux} as
\begin{equation}
    S = n h \nu
\end{equation}
\(n =\) number of light quanta passing through per unit area per unit time.

\paragraph{Energy Flux (by Maxwell's equation):}
After solving the wave equation for a monochromatic light beam (frequency \(\nu\)) along $z$-direction.
\begin{equation*}
    \vb{E} = \vb{E}_\circ \cos(k z - \omega t) \qquad \qquad \qquad \vb{B} = \frac{\vb{E}_\circ}{c} \cos(k z - \omega t)
\end{equation*}
Using this field, we can calculate energy flux (given by the Poynting vector)
\begin{equation}
    \vb{S} = \frac{1}{\mu_\circ} \qty(\vb{E} \cp \vb{B}) = \frac{E_\circ^2}{\mu_\circ c} \cos[2](k z - \omega t) ~\k
\end{equation}
Time-averaged flux
\begin{equation}
    \expval{\vb{S}} = \frac{E_\circ^2}{2 \mu_\circ c} = \frac{\eps_\circ c E_\circ^2}{2}
\end{equation}

\begin{exercise} \label{ex:electric-capacitor}
    Compute the electric field between two parallel plates that are separated by \(1 ~ \mathrm{cm}\) and connected to a \(5 ~\mathrm{V}\) battery.
\end{exercise}
\begin{solution}
    Using \(\vb{E} = -\grad{V}\),\vspace{-2ex}
    \begin{gather*}
        V = \int \vb{E} \vdot \dd{\vb{r}} = Er \\
        E = \frac{V}{r} = 500 ~\mathrm{V/m}
    \end{gather*}
\end{solution}

\begin{exercisebox}
    \begin{exercise}[Assignment-02/Ex-01] \label{ex:electric-led}
        Compute the peak electric field due to a light beam generated by a \(5 ~\mathrm{W}\) LED blub and passing through a square of side \(10 ~\mathrm{cm}\).
    \end{exercise}
\end{exercisebox}

\begin{exercisebox}
    \begin{exercise}[Assignment-02/Ex-02]
        In the two configurations (of \cref{ex:electric-capacitor} and \cref{ex:electric-led}), which one has a stronger peak electric field?
    \end{exercise}
\end{exercisebox}

\begin{exercise}
    If the LED bulb emits red light with frequency \(650 ~\mathrm{nm}\) in \cref{ex:electric-led} then as per Planck's proposal, how many light quanta pass through a square plate of side length \(10 ~\mathrm{cm}\) per sec.
\end{exercise}
\begin{solution}
    Given, \(\lambda = 650 ~\mathrm{nm}\) and area of the square plate is \(A = 100 ~\mathrm{cm^2}\). Recall \(P = 5 ~\mathrm{W}\).
    \begin{gather*}
        S = \frac{P}{A} = n \frac{h c}{\lambda} \\
        n = \frac{5 (650 \times 10^{-9})}{10^{-2} (6.63 \times 10^{-34}) (3 \times 10^8)} \\
        n = 1.6 \times 10^{21} ~\mathrm{m^{-2} s^{-1}}
    \end{gather*}
    So required quantity is given by \(N = n A = 1.6 \times 10^{-19} ~\mathrm{s^{-1}}\).
\end{solution}

\begin{exercisebox}
    \begin{exercise}[Assignment-02/Ex-03]
        Estimate the peak electric field caused by a single light quanta passing per second through the square plate.
    \end{exercise}
\end{exercisebox}

\subsection{Photo-Electric Effect}

If a light beam falls on a metal, it emits electrons from its surface.

\begin{list}{}{}
    \item[\bfseries Phillip Lenard (1902):] observed that the kinetic energy of the emitted electrons increases if the frequency of the incident light beam increases.

        \noindent This observation conflicts with Maxwell's equation that implies the kinetic energy of the emitted electrons should depend on the intensity (\(\propto E_\circ^2\)) of the light beam (not on frequency).

    \item[\bfseries Albert Einstein (1905):] Using Planck's idea of light quanta, the maximum kinetic energy of emitted electrons is given by
        \begin{equation}
            E_{\text{max}} = h \nu - \mathsf{W} \label{eq:photoelectric}
        \end{equation}
        \begin{itemize}[$\cdot$, noitemsep]
            \item \(E_{\text{max}} =\) maximum kinetic energy of electrons.
            \item \(h \nu =\) energy of a light quanta.
            \item \(\mathsf{W} =\) binding energy of the electron [work function].
        \end{itemize}
        This mathematical explanation matched with the observations as well. In 1921, Einstein was awarded the Nobel Prize for explaining the Photoelectric effect (not for the then-untested theory of relativity).
\end{list}

\begin{exercise}
    In a photoelectric experiment, the maximum kinetic energy of an emitted electron was found to be \(0.86 ~\mathrm{eV}\) and \(0.37 ~\mathrm{eV}\) when corresponding incident lights were violet (\(400 ~\mathrm{nm}\)) and blue \(475 ~\mathrm{nm}\). Determine the value of the Planck's constant. Does the material show any photoelectric effect if one uses a red light beam (\(620 ~\mathrm{nm}\))?
\end{exercise}
\begin{solution}
    Using \cref{eq:photoelectric}, we can find \(h\) and then \(\mathsf{W}\).
    \begin{gather*}
        0.86 = \frac{h c}{400 \times 10^{-9}} - \phi_\circ \qquad \qquad \qquad \qquad 0.37 = \frac{h c}{475 \times 10^{-9}} - \phi_\circ \\
        0.49 = \frac{h c}{10^{-9}} \qty(\frac{1}{400} - \frac{1}{475}) \\
        \boxed{h = 4.14 \times 10^{-15} ~\mathrm{eV s}}
    \end{gather*}
\end{solution}

\subsection{Spectral Lines in Photo-Emission}

It was observed that the photo-emission spectrum of hydrogen atoms is not continuous (\ie only specific wavelengths are present).

\begin{list}{}{}
    \item[\bfseries Johannes Rydberg (1888):] He formulated a formula for all visible spectral lines (wavelengths) from hydrogen gas that can be expressed as
        \begin{equation}
            \frac{1}{\lambda} = R_H \qty(\frac{1}{2^2} - \frac{1}{n^2}), ~\forall ~n = 3, 4, 5, \dots
        \end{equation}
        \(R_H =\) Rydberg constant.

    \item[\bfseries Rutherford (1911):] Using his scattering experiment, he concluded an atom consists of concentrated +ve charge at the center, surrounded by -ve charged electron.
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.3\textwidth]{rutherford.png}
            \caption{Hydrogen atom according to Rutherford's model.}
            \label{fig:h-rutherford}
        \end{figure}
        According to the classical theory of electromagnetism (\ie Maxwell's equation), this configuration is unstable. Since it consists of an accelerating charged particle (\ie electron) revolving in a circular orbit, it must emit Electromagnetic waves.
        \paragraph{Classical analysis of this model:}
        We can do force balance,
        \begin{align}
            m_e \frac{v^2}{r} & = \frac{1}{4 \pi \eps_\circ} \frac{e^2}{r^2}
            \intertext{total energy of the electron is}
            E                 & = \text{KE} + \text{PE}\nonumber                                                                            \\
                              & = \half m_e v^2 - \frac{1}{4 \pi \eps_\circ} \frac{e^2}{r} \nonumber                                        \\
                              & = \half \qty(\frac{1}{4 \pi \eps_\circ} \frac{e^2}{r}) - \frac{1}{4 \pi \eps_\circ} \frac{e^2}{r} \nonumber \\
                              & = - \half \qty(\frac{1}{4 \pi \eps_\circ}) \frac{e^2}{r}
        \end{align}
        Since classically, there is no bound on \(r\), the energy of an electron is continuous, then the photo-emission spectrum should be continuous. However, this is not consistent with the experiment.

    \item[\bfseries Niels Bohr (1913):] Electrons stay only on those orbits where their angular momentum is quantized.
        \begin{gather}
            \boxed{m_e v r = n \hbar}, \qquad \qquad n \in \N \\
            m_e^2 v^2 r^2 = \underbrace{\frac{m_e}{4 \pi \eps_\circ} e^2 r = n^2 \hbar^2} \nonumber \\
            \boxed{\frac{1}{r_n} = \frac{1}{n^2} \frac{m_e e^2}{4 \pi \eps_\circ \hbar^2}} \\
            \boxed{E_n = - \frac{m_e}{2 \hbar^2} \qty(\frac{e^2}{4 \pi \eps_\circ})^2 \frac{1}{n^2}}
        \end{gather}
\end{list}
Now, if an electron jumps from \(n \in \qty{3, 4, 5, \dots}\) to \(n = 2\) energy level, then the energy of the emitted light quanta.
\begin{align}
    h \nu               & = E_n - E_2                                                                                                \\
    \frac{h c}{\lambda} & = \frac{m_e}{2 \hbar^2} \qty(\frac{e^2}{4 \pi \eps_\circ})^2 \qty[\frac{1}{2^2} - \frac{1}{n^2}] \nonumber \\
    \frac{1}{\lambda}   & = \frac{2 \pi^2 m_e}{h^3 c} \qty(\frac{e^2}{4 \pi \eps_\circ})^2 \qty[\frac{1}{2^2} - \frac{1}{n^2}]       \\
    \Aboxed{            & R_H = \frac{2 \pi^2 m_e}{h^3 c} \qty(\frac{e^2}{4 \pi \eps_\circ})^2}
\end{align}

\begin{exercisebox}
    \begin{exercise}[Assignment-03/Ex-01]
        An experimental physicist finds the wavelength of an EM wave from hydrogen gas to be \(1010 ~\mathrm{nm}\) with a spectrometer having an accuracy of \(1\%\). Using the Bohr model, determine the initial and final energy of the corresponding electron (Given \(E_1 = - 13.6 ~\mathrm{eV}\)).
    \end{exercise}
\end{exercisebox}

\subsection*{Summary of these three theories:}
\begin{enumerate}
    \item Energy of the electromagnetic waves quantized.
    \item An electron absorbs energy in quanta.
    \item An electron emits energy in quanta.
\end{enumerate}

\section{Determinism}

Consider a particle (classical particle) of mass \(m\) under a constant force \(F\). We can calculate the position of the particle by
\begin{equation*}
    x(t) = x_\circ + v_\circ t + \half \qty(\frac{F}{m}) t^2
\end{equation*}
So if we know the value of \(x_\circ\) and \(v_\circ\), we can precisely calculate the position of this particle.

\noindent {\color{purple} \emph{Is it possible to determine \(x_\circ\) and \(v_\circ\) of any particle even in principle?}}\\
We can break this question into two subquestions:
\begin{enumerate}
    \item How do we measure position (say \(x_\circ\))? \\[1.2ex]
          Send a light beam to measure the delay in arrival time, say \(t_\circ\) of the reflected wave.
          \begin{figure}[H]
              \centering
              \includegraphics[width=0.5\textwidth]{determinism-position.png}
              \begin{equation*}
                  \mbox{The inherent error in position measurement can be estimate as} ~\Delta x \sim \frac{\lambda}{2}
              \end{equation*}
              \caption{Experimental setup to measure the position of a particle from a defined origin, say \(O\)}
              \label{fig:determinism-position}
          \end{figure} \vspace{-2ex}\noindent
          So, we can estimate the position in terms of measured \(t_\circ\).
          \begin{equation*}
              x_\circ = \frac{c t_\circ}{2}
          \end{equation*}
          We should use smaller \(\lambda\) for measuring the position (to minimize the error).

    \item How do we measure velocity (say \(v_\circ\))? \\[1.2ex]
          Measure the position (say \(x_\circ\)) again after an time interval of \(t_\circ'\).
          \begin{equation*}
              v_\circ = \frac{x_\circ' - x_\circ}{t_\circ'}
          \end{equation*}
          Recall the momentum of a photon is given by \(h/\lambda\). The particle's original momentum ($p_\circ = m v_\circ$) changes to reflect the photon. \\
          The inherent error in momentum measurement can be estimated as \(\Delta p \sim \frac{2 h}{\lambda}\).\\
          We should use larger \(\lambda\) for measuring the momentum (to minimize the error).
\end{enumerate}

\noindent Using these errors, we can find an approximate error in the whole process of measurement by
\begin{equation}
    \boxed{\Delta x \Delta p \sim h} \label{eq:primitive-uncertainty}
\end{equation}
This expression implies that it doesn't matter what kind of light we measure the quantities. There will be an inherent error in the whole process of order \(h\).\\[1.2ex]
{\scshape \Cref{eq:primitive-uncertainty} is a primitive form of ``\textcolor{red}{Heisenberg's Uncertainty Principle}.''}

\noindent This argument implies that ``Determinism in classical physics depends on the conditions that can't be provided in general.''